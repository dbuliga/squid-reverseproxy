# Overview

Squid is a high-performance proxy caching server for web clients, supporting
FTP, gopher, and HTTP data objects.
 
Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
new features including ICAP and ESI support.

http://www.squid-cache.org/

# Usage

## General

This charm provides squid in a reverse proxy setup. 


http://en.wikipedia.org/wiki/Reverse_proxy

The most common scenario is to accelerate a web service:
You run squid on your outside edge, forwarding queries to
one or multiple internal web application servers.

The charm can be deployed in a single or multi-unit setup.

To deploy a single unit:

    juju deploy squid-reverseproxy

To add more units:

    juju add-unit squid-reverseproxy 

Example with apache:

    juju deploy apache2
    juju deploy squid-reverseproxy
    juju add-relation apache2:website-cache squid-reverseproxy:cached-website

This will put squid in front of apache2.

Once deployed, you can ssh into the deployed service:

    juju ssh <unit>

To list running units:

    juju status

To start monitoring Squid using Nagios:

    juju deploy nrpe-external-master
    juju add-relation squid-reverseproxy nrpe-external-master



This charm requires the following relation settings from clients:

    ip: service ip address
    port: service port
    sitenames: space-delimited list of virtual hosts to whitelist

The options that can be configured in config.yaml should be self-explanatory.
If not, please file a bug against this charm.

## HTTPS Reverse Proxying

Assuming you have a squid3 deb compiled with --enable-ssl, you can setup a
single https reverse proxy.

An example of this would be:

    juju set squid-reverseproxy enable_https=true ssl_key="$(base64 < /path/to/cert.key)" ssl_cert="$(base64 < /path/to/cert.crt)"

This should enable https access to the default website.

A current implementation limitation is that it doesn't support multiple https vhosts.

## Monitoring

This charm provides relations that support monitoring via Nagios using 
nrpe_external_master as a subordinate charm.

## Caveats
The example above is just for reference. In order to make it usable, you 
will have to supply a proper virtual host configuration for apache2.
