#!/usr/bin/python3
from base64 import b64encode

import amulet
import requests

seconds = 20000

d = amulet.Deployment(series='trusty')

d.add('apache2')
d.add('squid-reverseproxy')

d.configure('squid-reverseproxy', {
    'enable_https': True,
    'ssl_key': b64encode(open('cert.key').read()),
    'ssl_cert': b64encode(open('cert.cert').read())
})

d.relate('apache2:website-cache', 'squid-reverseproxy:cached-website')

d.expose('apache2')
d.expose('squid-reverseproxy')

try:
    d.setup(timeout=seconds)
except amulet.helpers.TimeoutError:
    amulet.raise_status(amulet.SKIP, msg="Environment wasn't stood up in time")
except:
    raise


##
# Set relationship aliases
##
squid_unit = d.sentry.unit['squid-reverseproxy/0']
apache_unit = d.sentry.unit['apache2/0']


def test_web_proxy():
    try:
        squid_request = requests.get(
            'https://%s/' % squid_unit.info['public-address'],
            cert=('cert.cert', 'cert.key')
        )
        apache_request = requests.get(
            'http://%s/' % apache_unit.info['public-address'])
        if not squid_request.ok or not apache_request.ok:
            amulet.raise_status(amulet.FAIL,
                                msg="Error connecting.")
        if squid_request.text != apache_request.text:
            amulet.raise_status(amulet.FAIL,
                                msg="Squid response different from apache.")
    except requests.exceptions.SSLError:
        amulet.raise_status(amulet.FAIL,
                            msg="SSL wrongly configured.")


test_web_proxy()
