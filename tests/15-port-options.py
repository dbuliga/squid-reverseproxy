#!/usr/bin/python3
import sys
import time
import json

import amulet
import requests

seconds = 20000

d = amulet.Deployment(series='trusty')

d.add('kibana')
d.add('elasticsearch')
d.add('logstash-indexer', charm='cs:precise/logstash-indexer')

d.add_unit('elasticsearch', 2)

d.relate('logstash-indexer:cluster', 'elasticsearch:client')
d.relate('kibana:rest', 'elasticsearch:client')

d.expose('kibana')

try:
    d.setup(timeout=seconds)
except amulet.helpers.TimeoutError:
    amulet.raise_status(amulet.SKIP, msg="Environment wasn't stood up in time")
except:
    raise


##
# Set relationship aliases
##
kibana_unit = d.sentry.unit['kibana/0']
elasticsearch_unit = d.sentry.unit['elasticsearch/1']


def test_web_interface():

    elasticsearch_unit.run("""
        curl -X POST 'http://127.0.0.1:9200/person/1' -d '{
        "info" : {
                "height" : 2,
                "width" : 20
        }
        }'
    """)

    time.sleep(3)

    url = 'http://%s/_all/_search' % kibana_unit.info['public-address']
    r = requests.get(url,
                     data=json.load(open('11-scale-elastic-query.json')))
    if not r.ok:
        amulet.raise_status(amulet.FAIL,
                            msg="Error connecting.")
    try:
        result = r.json()
        if result['hits']['total'] != 1:
            amulet.raise_status(amulet.FAIL,
                                msg="Error inserting value in elasticsearch.")
    except ValueError:
        exc_type, value, traceback = sys.exc_info()
        exc_text = "Exception: (%s) %s. %s" % (value, exc_type, traceback)
        amulet.raise_status(amulet.FAIL,
                            msg="Value returned not a valid json." + r.text +
                            exc_text)


test_web_interface()
