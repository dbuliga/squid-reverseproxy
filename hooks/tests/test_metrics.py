import sys
import textwrap
from types import ModuleType

from testtools import TestCase
from mock import patch, mock_open

import hooks

# import cron script as a standalone module
with open('files/squid_metrics.py') as script:
    source = script.read()
squid_metrics = ModuleType('squid_metrics.py')
sys.modules['squid_metrics'] = squid_metrics
exec source in squid_metrics.__dict__


class MetricsTestCase(TestCase):

    def add_patch(self, *args, **kwargs):
        p = patch(*args, **kwargs)
        self.addCleanup(p.stop)
        return p.start()

    def setUp(self):
        super(MetricsTestCase, self).setUp()

        self.open = mock_open()
        self.add_patch("hooks.open", self.open, create=True)
        self.copy2 = self.add_patch("shutil.copy2")
        self.config_get = self.add_patch("hooks.config_get")
        self.local_unit = self.add_patch("hooks.local_unit")
        self.log = self.add_patch("hooks.log")
        self.apt_install = self.add_patch("hooks.apt_install")

        self.config_get.return_value = {
            'metrics_sample_interval': 5,
            'metrics_scheme': 'scheme',
            'metrics_target': 'localhost:4321',
            'snmp_port': '1234',
            'snmp_community': 'community',
            'metrics': """ a b
                c """
        }
        self.local_unit.return_value = "unit/0"

    @patch('hooks.os.unlink')
    def test_write_metrics_cronjob_disabled_no_community(self, mock_unlink):
        self.config_get.return_value['snmp_community'] = ''
        hooks.write_metrics_cronjob('/script', '/config', '/cron')
        self.assertEqual(mock_unlink.mock_calls[0][1][0], '/config')
        self.assertEqual(mock_unlink.mock_calls[1][1][0], '/cron')

    @patch('hooks.os.unlink')
    def test_write_metrics_cronjob_disabled_no_target(self, mock_unlink):
        self.config_get.return_value['metrics_target'] = ''
        hooks.write_metrics_cronjob('/script', '/config', '/cron')
        self.assertEqual(mock_unlink.mock_calls[0][1][0], '/config')
        self.assertEqual(mock_unlink.mock_calls[1][1][0], '/cron')

    @patch('hooks.os.unlink')
    def test_write_metrics_cronjob_disabled_bad_target(self, mock_unlink):
        self.config_get.return_value['metrics_target'] = 'sadfsadf'
        hooks.write_metrics_cronjob('/script', '/config', '/cron')
        self.assertEqual(mock_unlink.mock_calls[0][1][0], '/config')
        self.assertEqual(mock_unlink.mock_calls[1][1][0], '/cron')

    def test_write_metrics_cronjob_enabled(self):
        self.config_get.return_value['metrics_target'] = 'localhost:4321'

        hooks.write_metrics_cronjob('/script', '/config', '/cron')

        # first open
        config_open_args = self.open.mock_calls[0][1]
        self.assertEqual(config_open_args, ('/config', 'w'))

        config_write = self.open.mock_calls[2][1][0]
        expected_config = textwrap.dedent("""
            [metrics]
            unit: unit/0
            scheme: scheme
            snmp_port: 1234
            statsd_hostport: localhost:4321
            community: community
            metrics: a,b,c
        """).strip()
        self.assertEqual(expected_config, config_write)

        # 2nd open
        cron_open_args = self.open.mock_calls[4][1]
        self.assertEqual(cron_open_args, ('/cron', 'w'))

        cron_write = self.open.mock_calls[6][1][0]
        expected_cron = textwrap.dedent("""
           # crontab for pushing squid metrics to statsd
           */5 * * * * root python /script send
        """).lstrip()
        self.assertEqual(expected_cron, cron_write)


class MetricsJobTestCase(TestCase):

    def add_patch(self, *args, **kwargs):
        p = patch(*args, **kwargs)
        self.addCleanup(p.stop)
        return p.start()

    def setUp(self):
        super(MetricsJobTestCase, self).setUp()

        self.config = {
            'unit': 'unit/0',
            'scheme': 'dev.$UNIT.$METRIC.5min',
            'snmp_port': '1234',
            'statsd_hostport': '127.0.0.1:8000',
            'community': 'public',
            'metrics': [],
        }

        self.snmp = self.add_patch('squid_metrics.get_snmp_value')
        patcher = patch.dict(squid_metrics.METRIC_TYPES, {
            'first': 'c',
            'second': 'c',
            'third.5': 'g',
            'fourth.60': 'g',
            'cachePeerRtt': 'g',
        })
        patcher.start()
        self.addCleanup(patcher.stop)

    def test_periods(self):

        self.config['metrics'] = [
            'first', 'second', 'third.5', 'fourth.60'
        ]

        self.snmp.side_effect = [
            squid_metrics.SNMPMetricFailed(),  # no cache peers
            'SQUID-MIB::first 1',
            'SQUID-MIB::second.0 2',
            'SQUID-MIB::third.5 3',
            'SQUID-MIB::fourth.60 4',
        ]

        metrics = list(squid_metrics.get_metrics(self.config))

        self.assertEqual(metrics, [
            ('dev.unit-0.first.5min', '1', 'c'),
            ('dev.unit-0.second.5min', '2', 'c'),
            ('dev.unit-0.third-5.5min', '3', 'g'),
            ('dev.unit-0.fourth-60.5min', '4', 'g'),
        ])

    def test_peers_with_names(self):
        self.config['metrics'] = [
            'cachePeerRtt'
        ]

        self.snmp.side_effect = [
            (
                'SQUID-MIB::cachePeerName.1 one.com\n' +
                'SQUID-MIB::cachePeerName.2 two.org\n' +
                'SQUID-MIB::cachePeerName.3 three.net\n'
            ),
            (
                'SQUID-MIB::cachePeerRtt.1 1\n' +
                'SQUID-MIB::cachePeerRtt.2 2\n' +
                'SQUID-MIB::cachePeerRtt.3 3\n'
            ),
        ]

        metrics = list(squid_metrics.get_metrics(self.config))

        self.assertEqual(metrics, [
            ('dev.unit-0.cachePeerRtt.one-com.5min', '1', 'g'),
            ('dev.unit-0.cachePeerRtt.two-org.5min', '2', 'g'),
            ('dev.unit-0.cachePeerRtt.three-net.5min', '3', 'g'),
        ])

    def test_peers_without_names(self):
        self.config['metrics'] = [
            'cachePeerRtt'
        ]

        self.snmp.side_effect = [
            squid_metrics.SNMPMetricFailed(),
            (
                'SQUID-MIB::cachePeerRtt.1 1\n' +
                'SQUID-MIB::cachePeerRtt.2 2\n' +
                'SQUID-MIB::cachePeerRtt.3 3\n'
            ),
        ]

        metrics = list(squid_metrics.get_metrics(self.config))

        self.assertEqual(metrics, [
            ('dev.unit-0.cachePeerRtt.1.5min', '1', 'g'),
            ('dev.unit-0.cachePeerRtt.2.5min', '2', 'g'),
            ('dev.unit-0.cachePeerRtt.3.5min', '3', 'g'),
        ])
