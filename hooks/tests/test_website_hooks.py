import yaml

from testtools import TestCase
from mock import patch

import hooks
from charmhelpers.core.hookenv import Serializable


class WebsiteRelationTest(TestCase):

    def setUp(self):
        super(WebsiteRelationTest, self).setUp()

        relations_of_type = patch.object(hooks, "relations_of_type")
        self.relations_of_type = relations_of_type.start()
        self.addCleanup(relations_of_type.stop)

        config_get = patch.object(hooks, "config_get")
        self.config_get = config_get.start()
        self.addCleanup(config_get.stop)

        log = patch.object(hooks, "log")
        self.log = log.start()
        self.addCleanup(log.stop)

    def test_relation_data_returns_no_relations(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = []
        self.assertEqual(None, hooks.get_reverse_sites())

    def test_no_port_in_relation_data(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "__unit__": "foo/1",
             "__relid__": "website:1"},
            ]
        self.assertIs(None, hooks.get_reverse_sites())
        self.log.assert_called_once_with(
            "No port in relation data for 'foo/1', skipping.")

    def test_empty_relation_services(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "__unit__": "foo/1",
             "__relid__": "website:1",
             "all_services": "",
             },
            ]

        self.assertEqual(None, hooks.get_reverse_sites())
        self.assertFalse(self.log.called)

    def test_no_port_in_relation_data_ok_with_all_services(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "__unit__": "foo/1",
             "__relid__": "website:1",
             "all_services": yaml.dump([
                 {"service_name": "foo.internal",
                  "service_port": 4242},
                 ]),
             },
            ]

        expected = {
            "foo.internal": [
                ("foo_internal__website_1__foo_1", "1.2.3.4", 4242, ''),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())
        self.assertFalse(self.log.called)

    def test_no_private_address_in_relation_data(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"port": 4242,
             "__unit__": "foo/0",
             "__relid__": "website:1",
             },
            ]
        self.assertIs(None, hooks.get_reverse_sites())
        self.log.assert_called_once_with(
            "No private-address in relation data for 'foo/0', skipping.")

    def test_sitenames_in_relation_data(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "port": 4242,
             "__unit__": "foo/1",
             "__relid__": "website:1",
             "sitenames": "foo.internal bar.internal"},
            {"private-address": "1.2.3.5",
             "port": 4242,
             "__unit__": "foo/2",
             "__relid__": "website:1",
             "sitenames": "foo.internal bar.internal"},
            ]
        expected = {
            "foo.internal": [
                ("foo_internal__website_1__foo_1", "1.2.3.4", 4242, ''),
                ("foo_internal__website_1__foo_2", "1.2.3.5", 4242, ''),
                ],
            "bar.internal": [
                ("bar_internal__website_1__foo_1", "1.2.3.4", 4242, ''),
                ("bar_internal__website_1__foo_2", "1.2.3.5", 4242, ''),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())

    def test_all_services_in_relation_data(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "__unit__": "foo/1",
             "__relid__": "website:1",
             "all_services": yaml.dump([
                 {"service_name": "foo.internal",
                  "service_port": 4242},
                 {"service_name": "bar.internal",
                  "service_port": 4243}
                 ]),
             },
            {"private-address": "1.2.3.5",
             "__unit__": "foo/2",
             "__relid__": "website:1",
             "all_services": yaml.dump([
                 {"service_name": "foo.internal",
                  "service_port": 4242},
                 {"service_name": "bar.internal",
                  "service_port": 4243}
                 ]),
             },
            ]
        expected = {
            "foo.internal": [
                ("foo_internal__website_1__foo_1", "1.2.3.4", 4242, ''),
                ("foo_internal__website_1__foo_2", "1.2.3.5", 4242, ''),
                ],
            "bar.internal": [
                ("bar_internal__website_1__foo_1", "1.2.3.4", 4243, ''),
                ("bar_internal__website_1__foo_2", "1.2.3.5", 4243, ''),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())

    def test_unit_names_in_relation_data(self):
        self.config_get.return_value = Serializable({})
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "__relid__": "website_1",
             "__unit__": "foo/1",
             "port": 4242},
            {"private-address": "1.2.3.5",
             "__relid__": "website_1",
             "__unit__": "foo/2",
             "port": 4242},
            ]
        expected = {
            None: [
                ("website_1__foo_1", "1.2.3.4", 4242, ''),
                ("website_1__foo_2", "1.2.3.5", 4242, ''),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())

    def test_sites_from_config_no_domain(self):
        self.relations_of_type.return_value = []
        self.config_get.return_value = Serializable({
            "services": yaml.dump([
                {"service_name": "foo.internal",
                 "servers": [
                     ["1.2.3.4", 4242],
                     ["1.2.3.5", 4242],
                     ]
                 }
                ])})
        expected = {
            "foo.internal": [
                ("foo_internal__1_2_3_4", "1.2.3.4", 4242, ''),
                ("foo_internal__1_2_3_5", "1.2.3.5", 4242, ''),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())

    def test_sites_from_config_with_domain(self):
        self.relations_of_type.return_value = []
        self.config_get.return_value = Serializable({
            "services": yaml.dump([
                {"service_name": "foo.internal",
                 "server_options": ["forceddomain=example.com"],
                 "servers": [
                     ["1.2.3.4", 4242],
                     ["1.2.3.5", 4242],
                     ]
                 }
                ])})
        expected = {
            "foo.internal": [
                ("foo_internal__1_2_3_4", "1.2.3.4", 4242,
                 "forceddomain=example.com"),
                ("foo_internal__1_2_3_5", "1.2.3.5", 4242,
                 "forceddomain=example.com"),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())

    def test_sites_from_config_and_relation_with_domain(self):
        self.relations_of_type.return_value = [
            {"private-address": "1.2.3.4",
             "__unit__": "foo/1",
             "__relid__": "website:1",
             "all_services": yaml.dump([
                 {"service_name": "foo.internal",
                  "service_port": 4242},
                 {"service_name": "bar.internal",
                  "service_port": 4243}
                 ]),
             },
            {"private-address": "1.2.3.5",
             "__unit__": "foo/2",
             "__relid__": "website:1",
             "all_services": yaml.dump([
                 {"service_name": "foo.internal",
                  "service_port": 4242},
                 {"service_name": "bar.internal",
                  "service_port": 4243}
                 ]),
             },
            ]
        self.config_get.return_value = Serializable({
            "services": yaml.dump([
                {"service_name": "foo.internal",
                 "server_options": ["forceddomain=example.com"],
                 "servers": [
                     ["1.2.4.4", 4242],
                     ["1.2.4.5", 4242],
                     ]
                 }
                ])})
        expected = {
            "foo.internal": [
                ("foo_internal__1_2_4_4", "1.2.4.4", 4242,
                 "forceddomain=example.com"),
                ("foo_internal__1_2_4_5", "1.2.4.5", 4242,
                 "forceddomain=example.com"),
                ("foo_internal__website_1__foo_1", "1.2.3.4", 4242,
                 "forceddomain=example.com"),
                ("foo_internal__website_1__foo_2", "1.2.3.5", 4242,
                 "forceddomain=example.com"),
                ],
            "bar.internal": [
                ("bar_internal__website_1__foo_1", "1.2.3.4", 4243, ''),
                ("bar_internal__website_1__foo_2", "1.2.3.5", 4243, ''),
                ],
            }
        self.assertEqual(expected, hooks.get_reverse_sites())

    def test_empty_sites_from_config_no_domain(self):
        self.relations_of_type.return_value = []
        self.config_get.return_value = Serializable({
            "services": ""})
        self.assertEqual(None, hooks.get_reverse_sites())
