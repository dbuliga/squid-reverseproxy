from testtools import TestCase
from mock import patch, call

import hooks


class CachedWebsiteRelationTest(TestCase):

    def setUp(self):
        super(CachedWebsiteRelationTest, self).setUp()
        self.notify_cached_website = self.patch_hook("notify_cached_website")
        self.config_changed = self.patch_hook('config_changed')
        self.config_get = self.patch_hook('config_get')
        self.config_get.return_value = {
            'enable_forward_proxy': False
        }

    def patch_hook(self, hook_name):
        mock_controller = patch.object(hooks, hook_name)
        mock = mock_controller.start()
        self.addCleanup(mock_controller.stop)
        return mock

    def test_website_interface_none(self):
        self.assertEqual(None, hooks.cached_website_interface(hook_name=None))
        self.assertFalse(self.notify_cached_website.called)

    def test_website_interface_joined(self):
        hooks.cached_website_interface(hook_name="joined")
        self.notify_cached_website.assert_called_once_with(
            relation_ids=(None,))
        self.assertFalse(self.config_changed.called)

    def test_website_interface_changed(self):
        hooks.cached_website_interface(hook_name="changed")
        self.notify_cached_website.assert_called_once_with(
            relation_ids=(None,))
        self.assertFalse(self.config_changed.called)

    def test_website_interface_forward_enabled(self):
        self.config_get.return_value = {
            'enable_forward_proxy': True
        }
        hooks.cached_website_interface(hook_name="changed")
        self.notify_cached_website.assert_called_once_with(
            relation_ids=(None,))
        self.assertTrue(self.config_changed.called)


class NotifyCachedWebsiteRelationTest(TestCase):

    def setUp(self):
        super(NotifyCachedWebsiteRelationTest, self).setUp()

        self.get_service_ports = self.patch_hook("get_service_ports")
        self.get_sitenames = self.patch_hook("get_sitenames")
        self.get_relation_ids = self.patch_hook("get_relation_ids")
        self.get_hostname = self.patch_hook("get_hostname")
        self.relation_set = self.patch_hook("relation_set")

    def patch_hook(self, hook_name):
        mock_controller = patch.object(hooks, hook_name)
        mock = mock_controller.start()
        self.addCleanup(mock_controller.stop)
        return mock

    def test_notify_cached_website_no_relation_ids(self):
        self.get_relation_ids.return_value = ()

        hooks.notify_cached_website()

        self.assertFalse(self.relation_set.called)
        self.get_relation_ids.assert_called_once_with(
            "cached-website")

    def test_notify_cached_website_with_default_relation(self):
        self.get_relation_ids.return_value = ()
        self.get_hostname.return_value = "foo.local"
        self.get_service_ports.return_value = (3128,)
        self.get_sitenames.return_value = ("foo.internal", "bar.internal")

        hooks.notify_cached_website(relation_ids=(None,))

        self.get_hostname.assert_called_once_with()
        self.relation_set.assert_called_once_with(
            relation_id=None, port=3128,
            hostname="foo.local",
            sitenames="foo.internal bar.internal")
        self.assertFalse(self.get_relation_ids.called)

    def test_notify_cached_website_with_relations(self):
        self.get_relation_ids.return_value = ("cached-website:1",
                                              "cached-website:2")
        self.get_hostname.return_value = "foo.local"
        self.get_service_ports.return_value = (3128,)
        self.get_sitenames.return_value = ("foo.internal", "bar.internal")

        hooks.notify_cached_website()

        self.get_hostname.assert_called_once_with()
        self.get_relation_ids.assert_called_once_with("cached-website")
        self.relation_set.assert_has_calls([
            call.relation_set(
                relation_id="cached-website:1", port=3128,
                hostname="foo.local",
                sitenames="foo.internal bar.internal"),
            call.relation_set(
                relation_id="cached-website:2", port=3128,
                hostname="foo.local",
                sitenames="foo.internal bar.internal"),
            ])
