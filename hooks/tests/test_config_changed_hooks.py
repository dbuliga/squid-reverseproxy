from testtools import TestCase
from mock import patch

import hooks


class ConfigChangedTest(TestCase):

    def setUp(self):
        super(ConfigChangedTest, self).setUp()
        self.get_service_ports = self.patch_hook("get_service_ports")
        self.get_sitenames = self.patch_hook("get_sitenames")
        self.construct_squid3_config = self.patch_hook(
            "construct_squid3_config")
        self.update_nrpe_checks = self.patch_hook(
            "update_nrpe_checks")
        self.update_service_ports = self.patch_hook(
            "update_service_ports")
        self.service_squid3 = self.patch_hook(
            "service_squid3")
        self.notify_cached_website = self.patch_hook("notify_cached_website")
        self.log = self.patch_hook("log")
        self.config_get = self.patch_hook("config_get")
        self.write_metrics_cronjob = self.patch_hook("write_metrics_cronjob")

    def patch_hook(self, hook_name):
        mock_controller = patch.object(hooks, hook_name)
        mock = mock_controller.start()
        self.addCleanup(mock_controller.stop)
        return mock

    def test_config_changed_notify_cached_website_changed_stanzas(self):
        self.service_squid3.return_value = True
        self.get_sitenames.side_effect = (
            ('foo.internal',),
            ('foo.internal', 'bar.internal'))

        hooks.config_changed()

        self.notify_cached_website.assert_called_once_with()

    def test_config_changed_no_notify_cached_website_not_changed(self):
        self.service_squid3.return_value = True
        self.get_sitenames.side_effect = (
            ('foo.internal',),
            ('foo.internal',))

        hooks.config_changed()

        self.assertFalse(self.notify_cached_website.called)

    @patch("sys.exit")
    def test_config_changed_no_notify_cached_website_failed_check(self, exit):
        self.service_squid3.return_value = False
        self.get_sitenames.side_effect = (
            ('foo.internal',),
            ('foo.internal', 'bar.internal'))

        hooks.config_changed()

        self.assertFalse(self.notify_cached_website.called)
