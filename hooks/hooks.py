#!/usr/bin/env python

import math
import os
import random
import re
import shutil
import string
import subprocess
import sys
import yaml
import collections
import socket
import apt_pkg
import glob

from charmhelpers.core.hookenv import (
    config as config_get,
    log,
    relation_set,
    relation_get,
    relation_ids as get_relation_ids,
    relations_of_type,
    related_units,
    open_port,
    close_port,
    local_unit,
)
from charmhelpers.fetch import apt_install
from charmhelpers.contrib.charmsupport.nrpe import NRPE

###############################################################################
# Global variables
###############################################################################
default_squid3_config_dir = "/etc/squid3"
default_squid3_config = "%s/squid.conf" % default_squid3_config_dir
default_squid3_config_cache_dir = "/var/run/squid3"
default_nagios_plugin_dir = "/usr/lib/nagios/plugins"
metrics_cronjob_path = "/etc/cron.d/squid_metrics"
metrics_script_path = "/usr/local/bin/squid_metrics.py"
metrics_config_path = "/usr/local/etc/squid_metrics.ini"
site_mib_path = "/usr/share/mibs/site"
Server = collections.namedtuple("Server", "name address port options")
service_affecting_packages = ['squid3']

###############################################################################
# Supporting functions
###############################################################################


def get_id(sitename, relation_id, unit_name):
    unit_name = unit_name.replace('/', '_').replace('.', '_')
    relation_id = relation_id.replace(':', '_').replace('.', '_')
    if sitename is None:
        return relation_id + '__' + unit_name
    return sitename.replace('.', '_') + '__' + relation_id + '__' + unit_name


def get_forward_sites():
    reldata = []
    for relid in get_relation_ids('cached-website'):
        units = related_units(relid)
        for unit in units:
            data = relation_get(rid=relid, unit=unit)
            if 'sitenames' in data:
                data['sitenames'] = data['sitenames'].split()
            data['relation-id'] = relid
            data['name'] = unit.replace("/", "_")
            reldata.append(data)
    return reldata


def get_reverse_sites():
    all_sites = {}

    config_data = config_get()
    config_services = yaml.safe_load(config_data.get("services", "")) or ()
    server_options_by_site = {}

    for service_item in config_services:
        site = service_item["service_name"]
        servers = all_sites.setdefault(site, [])
        options = " ".join(service_item.get("server_options", []))
        server_options_by_site[site] = options
        for host, port in service_item["servers"]:
            servers.append(
                Server(name=get_id(None, site, host),
                       address=host, port=port,
                       options=options))

    relations = relations_of_type("website")

    for relation_data in relations:
        unit = relation_data["__unit__"]
        relation_id = relation_data["__relid__"]
        if not ("port" in relation_data or "all_services" in relation_data):
            log("No port in relation data for '%s', skipping." % unit)
            continue

        if "private-address" not in relation_data:
            log("No private-address in relation data "
                "for '%s', skipping." % unit)
            continue

        for site in relation_data.get("sitenames", "").split():
            servers = all_sites.setdefault(site, [])
            servers.append(
                Server(name=get_id(site, relation_id, unit),
                       address=relation_data["private-address"],
                       port=relation_data["port"],
                       options=server_options_by_site.get(site, '')))

        services = yaml.safe_load(relation_data.get("all_services", "")) or ()
        for service_item in services:
            site = service_item["service_name"]
            servers = all_sites.setdefault(site, [])
            servers.append(
                Server(name=get_id(site, relation_id, unit),
                       address=relation_data["private-address"],
                       port=service_item["service_port"],
                       options=server_options_by_site.get(site, '')))

        if not ("sitenames" in relation_data or
                "all_services" in relation_data):
            servers = all_sites.setdefault(None, [])
            servers.append(
                Server(name=get_id(None, relation_id, unit),
                       address=relation_data["private-address"],
                       port=relation_data["port"],
                       options=server_options_by_site.get(None, '')))

    if len(all_sites) == 0:
        return

    for site, servers in all_sites.iteritems():
        servers.sort()

    return all_sites


def ensure_package_status(packages, status):
    if status in ['install', 'hold']:
        selections = ''.join(['{} {}\n'.format(package, status)
                              for package in packages])
        dpkg = subprocess.Popen(['dpkg', '--set-selections'],
                                stdin=subprocess.PIPE)
        dpkg.communicate(input=selections)


def load_squid3_config(squid3_config_file="/etc/squid3/squid.conf"):
    """Convenience function that loads (as a string) the current squid3
    configuration file.

    Returns a string containing the squid3 config or None"""
    if os.path.isfile(squid3_config_file):
        return open(squid3_config_file).read()
    return


def get_service_ports(squid3_config_file="/etc/squid3/squid.conf"):
    """Convenience function that scans the existing squid3 configuration
    file and returns a list of the existing ports being used.

    This is necessary to know which ports to open and close when
    exposing/unexposing a service"""
    squid3_config = load_squid3_config(squid3_config_file)
    if squid3_config is None:
        return
    return re.findall("http_port ([0-9]+)", squid3_config)


def get_sitenames(squid3_config_file="/etc/squid3/squid.conf"):
    """Convenience function that scans the existing squid3 configuration
    file and returns a list of the sitenames being served by the instance."""
    squid3_config = load_squid3_config(squid3_config_file)
    if squid3_config is None:
        return
    sitenames = re.findall("acl [\w_-]+ dstdomain ([^!].*)", squid3_config)
    return list(set(sitenames))


def update_service_ports(old_service_ports=None, new_service_ports=None):
    """Convenience function that evaluate the old and new service ports to
    decide which ports need to be opened and which to close"""
    if old_service_ports is None or new_service_ports is None:
        return
    for port in old_service_ports:
        if port not in new_service_ports:
            close_port(port)
    for port in new_service_ports:
        if port not in old_service_ports:
            open_port(port)


def pwgen(pwd_length=20):
    """Generates a random password
    pwd_length:  Defines the length of the password to generate
                 default: 20"""
    alphanumeric_chars = [l for l in (string.letters + string.digits)
                          if l not in 'Iil0oO1']
    random_chars = [random.choice(alphanumeric_chars)
                    for i in range(pwd_length)]
    return ''.join(random_chars)


def render_template(template_name, vars):
    # deferred import so install hook can install jinja2
    from jinja2 import Environment, FileSystemLoader
    templates_dir = os.path.join(os.environ['CHARM_DIR'], 'templates')
    template_env = Environment(loader=FileSystemLoader(templates_dir))
    template = template_env.get_template(template_name)
    return template.render(vars)


def construct_squid3_config():
    """Convenience function to write squid.conf"""
    config_data = config_get()
    reverse_sites = get_reverse_sites()
    only_direct = set()
    if reverse_sites is not None:
        for site, servers in reverse_sites.iteritems():
            if not servers:
                only_direct.add(site)

    if config_data['refresh_patterns']:
        refresh_patterns = yaml.safe_load(config_data['refresh_patterns'])
    else:
        refresh_patterns = {}
    # Use default refresh pattern if specified.
    if '.' in refresh_patterns:
        default_refresh_pattern = refresh_patterns.pop('.')
    else:
        default_refresh_pattern = {
            'min': 30,
            'percent': 20,
            'max': 4320,
            'options': [],
            }

    config_data['cache_l1'] = int(math.ceil(math.sqrt(
        int(config_data['cache_size_mb']) * 1024 / (
            16 * int(config_data['target_objs_per_dir']) * int(
                config_data['avg_obj_size_kb'])))))
    config_data['cache_l2'] = config_data['cache_l1'] * 16
    apt_pkg.init()
    cache = apt_pkg.Cache()
    pkg = cache['squid3']
    # From 3.2 the manager, localhost, and to_localhost ACL definitions are now
    # built-in. So, detect the package version and remove the definitions from
    # the generated config
    need_localacl_defs = apt_pkg.version_compare(pkg.current_ver.ver_str,
                                                 '3.2')
    templ_vars = {
        'config': config_data,
        'sites': reverse_sites,
        'forward_relations': get_forward_sites(),
        'only_direct': only_direct,
        'refresh_patterns': refresh_patterns,
        'default_refresh_pattern': default_refresh_pattern,
        'need_localacl_defs': need_localacl_defs,
        'visible_hostname': get_hostname(),
    }
    template = render_template('main_config.template', templ_vars)
    write_squid3_config('\n'.join(
        (l.strip() for l in str(template).splitlines())))

    # If we have https, write out ssl key and cert file
    if config_data.get('enable_https'):
        from base64 import b64decode
        if config_data.get('ssl_key') is not None:
            ssl_keyfile = config_data['ssl_keyfile']
            ssl_keyfile_dir = os.path.dirname(os.path.realpath(ssl_keyfile))
            if not os.path.exists(ssl_keyfile_dir):
                os.makedirs(ssl_keyfile_dir)
            ssl_key = config_data['ssl_key']
            with open(ssl_keyfile, 'w') as ssl_key_path:
                ssl_key_path.write(str(b64decode(ssl_key)))
        if config_data.get('ssl_cert') is not None:
            ssl_certfile = config_data['ssl_certfile']
            ssl_certfile_dir = os.path.dirname(ssl_certfile)
            if not os.path.exists(ssl_certfile_dir):
                os.makedirs(ssl_certfile_dir)
            ssl_cert = config_data['ssl_cert']
            with open(ssl_certfile, 'w') as ssl_cert_path:
                ssl_cert_path.write(str(b64decode(ssl_cert)))


def write_squid3_config(contents):
    with open(default_squid3_config, 'w') as squid3_config:
        squid3_config.write(contents)


def delete_metrics_cronjob(config_path, cron_path):
    for path in (config_path, cron_path):
        try:
            os.unlink(path)
        except OSError:
            pass


def write_metrics_cronjob(script_path, config_path, cron_path):
    config_data = config_get()

    # need the following two configs to be valid
    metrics_target = config_data['metrics_target'].strip()
    snmp_community = config_data['snmp_community'].strip()
    if not snmp_community or not metrics_target or ':' not in metrics_target:
        log("Required config not found (snmp_community and metrics_target), "
            "disabling metrics")
        delete_metrics_cronjob(config_path, cron_path)
        return

    # check we can install the mibs ok
    try:
        apt_install('snmp-mibs-downloader', fatal=True)
    except subprocess.CalledProcessError:
        log("Could not install snmp-mibs-downloader package, "
            "disabling monitoring",
            level="ERROR")
        raise

    charm_dir = os.environ['CHARM_DIR']
    statsd_host, statsd_port = metrics_target.split(':', 1)
    metrics_list = [s.strip() for s in config_data['metrics'].split()]
    config_data['metrics_list'] = metrics_list
    config_data['unit_name'] = local_unit()

    # ensure script installed
    shutil.copy2('%s/files/squid_metrics.py' % charm_dir, metrics_script_path)

    # write the config
    with open(config_path, 'w') as config:
        config.write(
            render_template("squid_metrics_ini.template", config_data))

    # write the crontab
    with open(cron_path, 'w') as cronjob:
        cronjob.write(render_template("metrics_cronjob.template", {
            'interval': config_data['metrics_sample_interval'],
            'script': script_path,
            'config': config_path,
            'statsd_host': statsd_host,
            'statsd_port': statsd_port,
        }))


def service_squid3(action=None, squid3_config=default_squid3_config):
    """Convenience function to start/stop/restart/reload the squid3
    service"""
    if action is None or squid3_config is None:
        return
    elif action == "check":
        check_cmd = ['/usr/sbin/squid3', '-f', squid3_config, '-k', 'parse']
        return not subprocess.call(check_cmd)
    elif action == 'status':
        status = subprocess.check_output(['status', 'squid3'])
        return re.search('running', status) is not None
    elif action in ('start', 'stop', 'reload', 'restart'):
        return not subprocess.call([action, 'squid3'])


def install_nrpe_scripts():
    if not os.path.exists(default_nagios_plugin_dir):
        os.makedirs(default_nagios_plugin_dir)
    shutil.copy2('%s/files/check_squidpeers' % (
        os.environ['CHARM_DIR']),
        '{}/check_squidpeers'.format(default_nagios_plugin_dir))


def update_nrpe_checks():
    install_nrpe_scripts()
    nrpe_compat = NRPE()
    conf = nrpe_compat.config
    config_data = config_get()
    nrpe_compat.add_check(
        shortname='squidpeers',
        description='Check Squid Peers',
        check_cmd='check_squidpeers -p {port}'.format(**config_data)
    )
    check_http_params = conf.get('nagios_check_http_params')
    if check_http_params:
        nrpe_compat.add_check(
            shortname='squidrp',
            description='Check Squid',
            check_cmd='check_http %s' % check_http_params.format(**config_data)
        )
    check_https_params = conf.get('nagios_check_https_params')
    if check_https_params:
        nrpe_compat.add_check(
            shortname='squidrp_ssl',
            description='Check Squid SSL',
            check_cmd='check_http %s' %
            check_https_params.format(**config_data)
        )
    config_services_str = conf.get('services', '')
    config_services = yaml.safe_load(config_services_str) or ()
    for service in config_services:
        path = service.get('nrpe_check_path')
        http_scheme = service.get('http_scheme', 'http')
        if path is not None:
            method = "GET" if "?" in path else "HEAD"
            command = 'check_http -I 127.0.0.1 -p 3128 --method=%s ' % method
            service_name = service['service_name']
            if conf.get('x_balancer_name_allowed'):
                command += ("-u http://localhost%s "
                            "-k \\'X-Balancer-Name: %s\\'" % (
                                path, service_name))
            else:
                command += "-u %s://%s%s" % (http_scheme, service_name, path)
            nrpe_compat.add_check(
                shortname='squid-%s' % service_name.replace(".", "_"),
                description='Check Squid for site %s' % service_name,
                check_cmd=command,
                )
    nrpe_compat.write()


def install_packages():
    packages = "libecap2 libltdl7 libnetfilter-conntrack3 " \
                "ssl-cert libmnl0 squid-langpack libssl-dev " \
                "python-jinja2 snmp"
    apt_install(packages.split(), fatal=True)
    ensure_package_status(service_affecting_packages,
                          config_get('package_status'))
    config_data = config_get()
    squid_debs_url = config_data.get("squid_debs_url")
    archive_name = squid_debs_url.split("/")[
                len(squid_debs_url.split("/"))-1]
    subprocess.check_call(['wget', squid_debs_url])
    subprocess.check_call(['tar', '-xf', archive_name])
    subprocess.check_call(['dpkg', '-i'] + glob.glob('squid' + '*'))


###############################################################################
# Hook functions
###############################################################################
def install_hook():
    charm_dir = os.environ['CHARM_DIR']
    if not os.path.exists(default_squid3_config_dir):
        os.mkdir(default_squid3_config_dir, 0600)
    if not os.path.exists(default_squid3_config_cache_dir):
        os.mkdir(default_squid3_config_cache_dir, 0600)
    shutil.copy2('%s/files/default.squid3' % charm_dir, '/etc/default/squid3')
    install_packages()
    return True


def config_changed():
    old_service_ports = get_service_ports()
    old_sitenames = get_sitenames()
    construct_squid3_config()
    update_nrpe_checks()
    ensure_package_status(service_affecting_packages,
                          config_get('package_status'))

    write_metrics_cronjob(metrics_script_path,
                          metrics_config_path,
                          metrics_cronjob_path)

    if service_squid3("check"):
        updated_service_ports = get_service_ports()
        update_service_ports(old_service_ports, updated_service_ports)
        service_squid3("reload")
        if not (old_sitenames == get_sitenames()):
            notify_cached_website()
    else:
        # XXX Ideally the config should be restored to a working state if the
        # check fails, otherwise an inadvertent reload will cause the service
        # to be broken.
        log("squid configuration check failed, exiting")
        sys.exit(1)
    subprocess.call(["status-set", "active"])


def start_hook():
    if service_squid3("status"):
        return service_squid3("restart")
    else:
        return service_squid3("start")


def stop_hook():
    if service_squid3("status"):
        return service_squid3("stop")


def website_interface(hook_name=None):
    if hook_name is None:
        return
    if hook_name in ("joined", "changed", "broken", "departed"):
        config_changed()


def cached_website_interface(hook_name=None):
    if hook_name is None:
        return
    if hook_name in ("joined", "changed"):
        notify_cached_website(relation_ids=(None,))
    config_data = config_get()
    if config_data['enable_forward_proxy']:
        config_changed()


def get_hostname(host=None):
    my_host = socket.gethostname()
    if host is None or host == "0.0.0.0":
        # If the listen ip has been set to 0.0.0.0 then pass back the hostname
        return socket.getfqdn(my_host)
    elif host == "localhost":
        # If the fqdn lookup has returned localhost (lxc setups) then return
        # hostname
        return my_host
    return host


def notify_cached_website(relation_ids=None):
    hostname = get_hostname()
    # passing only one port - the first one defined
    port = get_service_ports()[0]
    sitenames = ' '.join(get_sitenames())

    for rid in relation_ids or get_relation_ids("cached-website"):
        relation_set(relation_id=rid, port=port,
                     hostname=hostname,
                     sitenames=sitenames)


def upgrade_charm():
    # Ensure that all current dependencies are installed.
    install_packages()


###############################################################################
# Main section
###############################################################################
def main(hook_name):
    if hook_name == "install":
        install_hook()
    elif hook_name == "config-changed":
        config_changed()
        update_nrpe_checks()
    elif hook_name == "start":
        start_hook()
    elif hook_name == "stop":
        stop_hook()
    elif hook_name == "upgrade-charm":
        upgrade_charm()
        config_changed()
        update_nrpe_checks()

    elif hook_name == "website-relation-joined":
        website_interface("joined")
    elif hook_name == "website-relation-changed":
        website_interface("changed")
    elif hook_name == "website-relation-broken":
        website_interface("broken")
    elif hook_name == "website-relation-departed":
        website_interface("departed")

    elif hook_name == "cached-website-relation-joined":
        cached_website_interface("joined")
    elif hook_name == "cached-website-relation-changed":
        cached_website_interface("changed")
    elif hook_name == "cached-website-relation-broken":
        cached_website_interface("broken")
    elif hook_name == "cached-website-relation-departed":
        cached_website_interface("departed")

    elif hook_name in ("nrpe-external-master-relation-joined",
                       "local-monitors-relation-joined"):
        update_nrpe_checks()

    elif hook_name == "env-dump":
        print relations_of_type()
    else:
        print "Unknown hook"
        sys.exit(1)

if __name__ == '__main__':
    hook_name = os.path.basename(sys.argv[0])
    # Also support being invoked directly with hook as argument name.
    if hook_name == "hooks.py":
        if len(sys.argv) < 2:
            sys.exit("Missing required hook name argument.")
        hook_name = sys.argv[1]
    main(hook_name)
